
## Description

_Please provide a description of the activity._


## Opportunity Assessment

_Why it is relevant to undertake this activity, What needs it addresses. What are the efforts expected. How much will it cost? What resources do we need? What RoI can be gained?_


## Progress Assessment

_Question: How can I assess if the activity is acquired? How will progress be measured. What are the objectives? What are the KPIs? Suggest verification point._


## Tools

_Technologies, tools and products concerned by this activity._

* [Wonderful Tool](https://alambic.io): a wonderful tool to do wonderful things.


## Recommendations

_Hints and best practices. Collected from GGI participants._

* Make people Happy.


## Resources

_Links to resources in the Resource Center_

* [How to achieve this activity](https://alambic.io): a great resource to achieve this activity.
